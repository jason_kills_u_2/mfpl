import lib 
import os
import sys

version = 2.0
platform = sys.platform

if len(sys.argv) == 2:
	if sys.argv[1] in ('-v', '--version'):
		print('MFPL {}\npPlatform = {}\nMFPL location = {}\nPython location = {}'.format(version, platform, sys.path[0], sys.path[2]))
		if platform == 'win32':
			os.system('echo | set /p="gcc: "')
			os.system('where gcc')
		elif platform in ('linux', 'linux2'):
			os.system('echo -n "gcc: "')
			os.system('which gcc')
		elif platform == 'darwin':
			os.system('echo -n "gcc:" ')
			os.system('where gcc')
	else:
		file = open(sys.argv[1], 'r')
		content = ''
		for i in file:
			content += i
		lib.Interpreter(content).interpret()
elif len(sys.argv) == 1:
	print('MFPL version {}'.format(version))
	while(True):
		content = input('MFPL>> ')
		if content == 'exit()':
			break
		elif content == '':
			continue
		lib.Interpreter(content).interpret()
